/*=============================================================================
|   Source code:  Application.c
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #1 Assignment 1
|
|        Course:  COP 4338
|       Section:  U03
|    Instructor:  William Feild
|      Due Date:  September 11, 2018, at the beginning of class
|
|	I hereby certify that this collective work is my own
|	and none of it is the work of any other person or entity.
|
|	___Alejandro Perez_________________________ [Signature]
|
|      Language:  C
|   Compile/Run:  Compile Application.c and then execute it
| 	gcc -o Application.out Application.c
|               ./Application.out
|  +-----------------------------------------------------------------------------
|
|   Description:  Based on the user input, specifying the steps size
|                 between each measurement, the program will print
|                 a formatted table displaying the conversion from miles
|                 to kilometers and viceversa
|
|         Input:  The steps size in which the measurements should be
|                 calculated. It must be an integer within the range [1-9]
|
|        Output:  A nice formatted table containing all the conversions
|                 in the steps specified by the user from 0 up to +320
|
|       Process:  The formula used to convert miles to kilometers and
|                 viceversa can be found here: https://www.metric-conversions.org/length/miles-to-kilometers.htm
|
|   Required Features Not Included: N/A
|
|   Known Bugs:  N/A
|  *===========================================================================*/

#include <stdio.h>
#include <stdlib.h> // to be able to use atoi to convert string to integer
#include <string.h> // to be able to use strlen to get a string length

/**
 * Constants Definition
 */
#define COLUMN_WIDTH 14
#define PRECISION 2
#define STDIN_LENGTH 80
#define CONVERSION_START 80
#define CONVERSION_END 400
#define CONVERSION_MILES_TO_KILOMETERS 1.60934
#define CONVERSION_KILOMETERS_TO_MILES 0.621371
#define EMPTY_LINE_AFTER_CONVERSIONS 8
#define LINE_SEPARATOR "_"

/**
 * Functions Definition
 */
int GetUserInputSteps();
void PrintConsole(char message[], char format[]);
void PrintFloat(float message, char format[]);
void PrintInt(int message);
void PrintProgramHeader();
void PrintString(char message[]);
void PrintTable(int steps);
void PrintTableHeader();
int ValidInput(int value);

/**
 * Program entry point
 *
 * @return Program execution status. If different
 * from 0 then something was not correct.
 */
int main()
{
    PrintProgramHeader();

    int steps = GetUserInputSteps();

    PrintTable(steps);

    return EXIT_SUCCESS;
}

/**
 * Helper to ceonvert kilometers to miles
 * 
 * @param kilometers The kilometers to be converted to miles
 */
float ConvertFromKilometersToMiles(int kilometers)
{
    return (float)kilometers * CONVERSION_KILOMETERS_TO_MILES;
}

/**
 * Helper to convert miles to kilometers
 * 
 * @param miles The miles to be converted to kilometers
 */
float ConvertFromMilesToKilometers(int miles)
{
    return (float)miles * CONVERSION_MILES_TO_KILOMETERS;
}

/**
 * Helper to obtain the user input steps value validated
 *
 * @return
 */
int GetUserInputSteps()
{
    int validInput = 0;
    int response;

    do
    {
        PrintString("Input the steps value [integer 1-9]: ");

        char temporalValue[STDIN_LENGTH];
        fgets(temporalValue, STDIN_LENGTH, stdin);
        response = atoi(temporalValue);

        validInput = ValidInput(response);

        if(!validInput)
            PrintString("\nSorry the value you provided is not valid try again.\n\n");

    } while (!validInput);

    PrintString("Step's size entered: ");
    PrintInt(response);
    PrintString("\n\n");

    return response;
}

/**
 * Helper from printing floats to the console
 *
 * @param message The float to be printed
 */
void PrintFloat(float message, char format[])
{
    printf(format ,message);
}

/**
 * Helper from printing integers to the console
 *
 * @param message The integer to be printed
 */
void PrintInt(int message)
{
    printf("%d" ,message);
}

/**
 * Print a description of what the program does to
 * the end user
 */
void PrintProgramHeader()
{
    PrintString("-------------------------------------------------------\n"
                "| This Application process all the conversions from   |\n"
                "| 0 to +320 of kilometers to miles and from miles     |\n"
                "| to kilometers. The values will be computed based    |\n"
                "| on your input. Provided a value for the steps size. |\n"
                "-------------------------------------------------------\n\n");
}

/**
 * Helper from printing strings to the console
 *
 * @param message The message to be printed
 */
void PrintString(char message[])
{
    printf("%s", message);
}

/**
 * Helper to process and print the result table
 * 
 * @param steps The steps used to determine the distance
 *              between each conversion
 */
void PrintTable(int steps)
{
    PrintTableHeader();

    int currentConversionValue = CONVERSION_START;
    int lineSeparationCounter = 0;

    while(currentConversionValue <= CONVERSION_END)
    {
        if(lineSeparationCounter > 0 && ((lineSeparationCounter % EMPTY_LINE_AFTER_CONVERSIONS) == 0)) {
            PrintString("\n");
        }
        
        printf("%*.*f", COLUMN_WIDTH, PRECISION, (float)currentConversionValue);
        printf("%*.*f", COLUMN_WIDTH, PRECISION, ConvertFromKilometersToMiles(currentConversionValue));
        printf("%*.*f", COLUMN_WIDTH, PRECISION, (float)currentConversionValue);
        printf("%*.*f", COLUMN_WIDTH, PRECISION, ConvertFromMilesToKilometers(currentConversionValue));
        PrintString("\n");

        lineSeparationCounter = lineSeparationCounter + 1;
        currentConversionValue = currentConversionValue + steps;
    }
}

/**
 * Helper to print out the header of the result table
 * 
 * NOTICE: In order to dynamically print the width of the
 * columns, we are using a concatenation of strings, the
 * source can be found here: https://stackoverflow.com/a/9448093/6922814
 */
void PrintTableHeader()
{
    printf("%*s%s", (int)(COLUMN_WIDTH - strlen("Kilomoters")), "", "Kilometers");
    printf("%*s%s", (int)(COLUMN_WIDTH - strlen("Miles")), "", "Miles");
    printf("%*s%s", (int)(COLUMN_WIDTH - strlen("Miles")), "", "Miles");
    printf("%*s%s", (int)(COLUMN_WIDTH - strlen("Kilometers")), "", "Kilometers");

    PrintString("\n");

    int lineSeparatorWidth = COLUMN_WIDTH * 4; // In this case 4 is the amount of columns
    int counter;

    for(counter = 0; counter < lineSeparatorWidth; counter = counter + 1) {
        PrintString(LINE_SEPARATOR);
    }

    PrintString("\n");
}

/**
 * Helper function to validate the user input
 *
 * @param value The value to validate
 * @return Whether the value is valid or not
 */
int ValidInput(int value)
{
    if(value > 0 && value < 10)
        return 1;

    return 0;
}